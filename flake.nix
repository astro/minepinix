{
  description = "Image builder for RaspberryPi";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/release-21.05";
    nixos-hardware.url = "github:nixos/nixos-hardware";
  };

  outputs = { self, nixpkgs, nixos-hardware }: {

    nixosModules = {
      defaults = import ./modules/defaults.nix;
      pi = import ./modules/pi.nix;
      pi4 = import ./modules/pi4.nix;
      minetest = import ./modules/minetest.nix;
    };

    nixosConfigurations = {
      pi4-minetest = nixpkgs.lib.nixosSystem {
        system = "aarch64-linux";
        modules = with self.nixosModules; [
          "${nixpkgs}/nixos/modules/installer/sd-card/sd-image-aarch64.nix"
          nixos-hardware.nixosModules.raspberry-pi-4
          defaults pi pi4 minetest
        ];
      };
    };

    packages =
      let
        systems =
          map (nixos:
            nixos.config.system.build.toplevel.system
          ) (builtins.attrValues self.nixosConfigurations);
      in builtins.foldl' (result: system: result // {
        "${system}" = nixpkgs.lib.mapAttrs (_: nixos:
          nixos.config.system.build.sdImage
        ) (
          nixpkgs.lib.filterAttrs (_: nixos:
            nixos.config.system.build.toplevel.system == system
          ) self.nixosConfigurations
        );
      }) {} systems;

  };
}
