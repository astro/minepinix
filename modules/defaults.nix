{ pkgs, ... }:
{
  nix = {
    buildCores = 4;
    maxJobs = 2;
  };
  networking = {
    useDHCP = false;
    interfaces.eth0.useDHCP = true;
    firewall.enable = false;
  };

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  users.users.pi = {
    isNormalUser = true;
    extraGroups = [ "wheel" "audio" "video" ];
    initialPassword = "";
  };

  # Select internationalisation properties.
  console = {
    font = "${pkgs.terminus_font}/share/consolefonts/ter-u28n.psf.gz";
    keyMap = "de";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.layout = "de";
  services.xserver.xkbOptions = "eurosign:e";

  services.xserver.displayManager = {
    lightdm = { enable = true; };
    autoLogin = {
      enable = true;
      user = "pi";
    };
  };
  services.xserver.desktopManager = {
    xfce.enable = true;
  };

  documentation = {
    enable = false;
    dev.enable = false;
    doc.enable = false;
    info.enable = false;
    man.enable = false;
    nixos.enable = false;
  };

  system.stateVersion = "21.05";
}
