{ pkgs, config, lib, ... }:
{
  sdImage = {
    compressImage = false;
    imageBaseName = config.networking.hostName;
    firmwareSize = 64;
  };

  boot = {
    loader = {
      grub.enable = false;
      generic-extlinux-compatible.enable = lib.mkDefault true;
      raspberryPi = {
        enable = true;
        uboot.enable = true;
        firmwareConfig = ''
          gpu_mem=192
          dtparam=audio=on
        '';
      };
    };
    kernelPackages = pkgs."linuxPackages_rpi${toString config.boot.loader.raspberryPi.version}";
    kernelParams = [
      "snd_bcm2835.enable_headphones=1"
    ];
    # Don't build ZFS for aarch64 (broken?)
    supportedFilesystems = lib.mkForce [ "vfat" "ext4" ];
    # Work around missing sun4i-drm.ko
    initrd.availableKernelModules = lib.mkForce [ "vc4" ];

    tmpOnTmpfs = true;
  };
  hardware.deviceTree = {
    enable = true;
    kernelPackage = config.boot.kernelPackages.kernel;
  };

  hardware.enableRedistributableFirmware = true;
  powerManagement.cpuFreqGovernor = lib.mkDefault "performance";

  environment.systemPackages = with pkgs; [
    libraspberrypi
    raspberrypi-eeprom
  ];

  # Do not log to sdcard:
  services.journald.extraConfig = ''
    Storage=volatile
  '';
}
