{ pkgs, ... }:
{
  networking.hostName = "minetest";

  environment.systemPackages = with pkgs; [
    minetest
  ];

  nixpkgs.config.packageOverrides = pkgs: {
    irrlicht = pkgs.irrlicht.overrideAttrs (oa: {
      CPPFLAGS="-DPNG_ARM_NEON_OPT=0";
      buildPhase = ''
        make -j$NIX_BUILD_CORES sharedlib NDEBUG=1 "LDFLAGS=-lX11 -lGL -lXxf86vm"
      '';
    });
    minetest = pkgs.minetest.overrideAttrs (oa: {
      enableParallelBuilding = true;
    });
  };
}
