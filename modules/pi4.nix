{ pkgs, ... }:
{
  boot.loader.raspberryPi.version = 4;
  hardware.raspberry-pi."4" = {
    fkms-3d.enable = true;
  };
}
